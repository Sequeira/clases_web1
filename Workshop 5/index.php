<?php
  session_start();
  if ($_SESSION && $_SESSION['usuario']){
    header('Location: dashboard.php');
  }

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'login':
        $message = 'User does not exists';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="./style.css">

  <title>Document</title>
</head>
<body>
<div class="container">
     <div class="msg">
      <?php echo $message; ?>
    </div>
    <div class ="login">
        <div class="row d-flex justify-content-center mx-auto">
            <div class="col-md-6 col-xs-12 div-style">
            <form  action="login.php" method="POST">
                <div class="d-flex justify-content-center mx-auto main-label" >
                    <h1>Login</h1>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control text-box" name="username" aria-describedby="" placeholder="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control text-box" name="password" placeholder="">
                </div>
                <div class="form-group justify-content-center d-flex">
                    <button type="submit"    class="btn btn-primary button-submit">Login</button>
                </div>
                <a class="btn btn-default" href="">Registrar cuenta</a>
            </form>
           </div>
        </div>
    </div>
</div>
</body>
</html>
