<?php

class DbConnection{
    private $user;
    private $password;
    private $database;
    private $port;
    private $server;
    public $activeConnection;
    function __construct($user, $password, $database, $port, $server,$activeConnection){
        $this->user     = $user;
        $this->password = $password;
        $this->database = $database;
        $this->port     = $port;
        $this->server   = $server;
        $this->activeConnection=$activeConnection;
        
    }

function getMySQLConnection(){
   if(!$activeConnection){
    $activeConnection = new mysqli($this->port,$this->user,$this->password,$this->database);    
   }
   if($activeConnection->connect_error){
    echo "Error" . $activeConnection->connect_error;
    exit();
   }
   return $activeConnection;
}
?>
}