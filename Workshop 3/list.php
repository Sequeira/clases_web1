<?php
$sql= 'SELECT * FROM categorias';
$connection = new mysqli('localhost', 'root', '', 'categoria');
$result = $connection->query($sql);
$categoria = $result->fetch_all();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
    <title>Lista de categorias</title>
</head>
<body>
    <div class="container">
        <nav class=" font-italic Italica font-weight-bold navbar navbar-light bg-light">
            <h1>Category</h1>
        </nav><br>
        <div class="table-responsive">
            <table id="userList" class="table table-bordered table-hover table-striped"><thead class="thead-light">
                <table class="table table-light table-striped table-hover">
                <tr>
                	<th scope="col">#</th>
			        <th scope="col">Name</th>
			        <th scope="col">Descripcion</th>
                    <th scope="col">Actions</th>
		            </tr>
		        </thead>
		        <tbody>
                    <?php

                    foreach($categoria as $categoria) {
                        echo "<tr><td>".$categoria[0].
                        "</td><td>".$categoria[1].
                        "</td><td>" .$categoria[2].
                        "</td><td>  <a href=\"editar.php?id=".$categoria[0]."\"> Editar </a>
                         |<a href=\"eliminar.php?id=".$categoria[0]."\"> Eliminar  </td></tr>";
                    } 
                    ?>
		        </tbody>
	        </table>
        </div>
         <?php
    ?>
    </div>
</body>
</html>
