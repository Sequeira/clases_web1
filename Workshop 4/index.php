<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css"> 
    <title>Formulario</title>
</head>

<?php
  include('functions.php');

  $career = getCareer();


?>
<body>
<div class="container"><br>
    <?php require ('header.php') ?><br>
    <h1>Form Registration</h1>
    <form action="save.php" method="POST" class="form-inline" role="form">
     <div class="form-group">
        <label class="sr-only" for="">Id</label>
        <input type="text" class="form-control" name="txt_ced" placeholder="Your ced"><br>
    </div>
    <div class="form-group">
        <label class="sr-only" for="">Name</label>
        <input type="text" class="form-control" name="txt_name" placeholder="Your Name"><br>
    </div>
    <div class="form-group">
        <label class="sr-only" for="">LastName</label>
        <input type="text" class="form-control" name="txt_lastname" placeholder="Your LastName"><br>
    </div>
    <div class="form-group">
        <label for="email">Email Address</label>
        <input id="email" class="form-control" type="text" name="txt_email"  placeholder="Your Email"><br>
    </div>
    <div class="form-group">
        <label for="career">Career</label>
        <select id="career" class="form-control" name="txt_career"  placeholder="Your Career">
          <?php
          foreach($career as $id => $career) {
            echo "<option value=\"$id\">$career</option>";
          }
          ?>
        </select><br>
    </div>
    <input type="submit" class="btn btn-primary" value="Submit"></input>
    </form>
</div>
</body>
</html>